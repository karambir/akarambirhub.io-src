Title: Lorem Markdownum
Date: 2016-02-03 10:20
Category: Markdown

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum

## Umbras illa vasta

Lorem markdownum tibi eburnas sagitta prementem similis sidus. Movet manu illis
secreta perpetiar sonat. In atque versatus tangit quid moles vox puerilibus et
rudis vanis requievit *nec nec* amato o undas sit. Color verba deus quae;
[crimen et](http://stoneship.org/) est nefanda famae. Illi ubi habetque,
hostibus addere, sors ad tantoque forte fulmine tempora concutit Pulchrior?

- Quae manu mixta illo odit coniunx virus
- Oculos caelestia ad albente aliquo
- Per retro quarum
- Appellantque hoc canibus talibus quoque huc velo
- Potestas ut magno sine longa cerva nec

## Simulacra non loquax etiamnum nescio glaebis leones

Petiisse pro inpia latus quoque carmina segetes feraxque coronatis tectae hic
bellis **nemorumque in littera** ostro prior de. O profuit moenia, erat quae
vulnera felix!

- Et timidi ab Ithaco declinet vincere
- Callida amissae frequentat namque
- Thracum nunc
- Quae est Iovem iterque utar levius pro
- Gremioque ficto fabricator virgineum tellus exsequitur spinae
- Repagula apta saecula absumitur adspicere et feret

## Vires capistris est modum et munusque tantum

Tam ipse Pelates Persea Saturno, adest per [turba](http://jaspervdj.be/) de
iunctim. [Excidit Clymene](http://www.lipsum.com/) argumenta et tenuit amare
forsitan primitias Pelasgos vocem: quem insolida agrestes exarsit. Dis vitam
[contudit](http://seenly.com/): fago Minyae dum!

```python
import os
import shutil
import sys
import SocketServer

from pelican.server import ComplexHTTPRequestHandler

# Local path configuration (can be absolute or relative to fabfile)
env.deploy_path = 'output'
DEPLOY_PATH = env.deploy_path

# Port for `serve`
PORT = 8000

def clean():
    """Remove generated files"""
    if os.path.isdir(DEPLOY_PATH):
        shutil.rmtree(DEPLOY_PATH)
        os.makedirs(DEPLOY_PATH)

def build():
    """Build local version of site"""
    local('pelican -s pelicanconf.py')

def rebuild():
    """`clean` then `build`"""
    clean()
    build()
```

## Pandion gelidi

Hac diu roseo lacum Cecropis quoque *olivae*. Per ab ementitus vesci manifesta.

1. Correptus ipsum
2. Tyranni oppida caelo fronti
3. Cubito Hippolytum fatalia ulta colebat vellem
4. Herbis sinistra
5. Fidem his

Aethera me vocat probatum caelestes latumque
[credideris](http://www.metafilter.com/) nova; perii erat aureus lues litora.
Fecundo fertur, tamquam [erat simulacra](http://imgur.com/) populos *noverca
tener*; aras cum abiere, adituque.
