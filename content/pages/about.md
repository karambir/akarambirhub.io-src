Title: About

My name is Karambir Singh Nain and I am a software developer.


## Website Setup

* Built using [Pelican](http://getpelican.com)
* Customized theme from [Pelican-Hyde](https://github.com/jvanz/pelican-hyde)
* Hosted on [GitHub Pages](https://pages.github.com)
* Checkout my [previous setup](https://github.com/akarambir/nainomics)

Suggestions are welcome on [Source Code](https://github.com/akarambir/akarambir.github.io-src)
