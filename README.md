## Source for akarambir.github.io
----

Built using [Pelican](http://getpelican.com/) with customized [Pelican-Hyde](https://github.com/jvanz/pelican-hyde) theme and served with [github pages](https://pages.github.com).

Inspired from  [jvanz personal site](https://github.com/jvanz/jvanz.github.io-src).
