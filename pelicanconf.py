#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Karambir Singh Nain'
SITENAME = 'Nainomics'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Asia/Kolkata'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = 'atom.xml'
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('github', 'https://github.com/akarambir'),
          ('twitter', 'https://twitter.com/akarambir'),
          ('facebook', 'https://facebook.com/akarambir'))

DEFAULT_PAGINATION = 12

THEME = "pelican-hyde"


STATIC_PATHS = ['images', 'extra/CNAME']
EXTRA_PATH_METADATA = {'extra/CNAME': {'path': 'CNAME'},}

BIO = "I'm Karambir, a Software Developer from New Delhi, India."
PROFILE_IMAGE = "avatar.jpg"

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
